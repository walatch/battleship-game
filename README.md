# **Battleship game simulator**

***

##  Battleship game main features:

- Standard Python libraries
- Network play using native Python sockets
- 3 modes of application: human client, autonomous client, autonomous host
- Built-in "AI" for autonomous modes to make shots based on previous shot results
- Built-in auto ship generator
- Replay functionality for autonomous modes

***

### Usage and startup:

Start the Battleship application using start.py
You will be prompted to select mode which you want to run and connection details. 
After a successful connection a game will begin.
Server always begins the game.

If you run an ai_client or ai_server, after the game you will be prompted to specify a replay interval for replaying each round (in seconds).

***

HAVE A BLAST!