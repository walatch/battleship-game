import validators as val
import game_mechanics as gm


class AiGunner:

    def __init__(self, board):
        # index type coords
        self.fired_shots = []
        self.shot_result = None
        self.current_shots = []
        self.current_hits = []
        self.current_possible_coords = set()
        self.current_ship_direction = (0, 0)
        self.board = board

    def next_shot(self):
        """ Generate next shot depending on shot_result last result """
        # for the first shot
        if not self.shot_result:
            return val.index_coord_to_str_coord(self.board, self.make_random_shot())

        if self.shot_result == "SINK":
            return val.index_coord_to_str_coord(self.board, self.if_sink())

        if self.shot_result == "HIT":
            return val.index_coord_to_str_coord(self.board, self.if_hit())

        if self.shot_result == "MISS":
            return val.index_coord_to_str_coord(self.board, self.if_miss())

    def if_sink(self):
        """ Add sank ship + padding to fired_shots to prevent empty shots and return random shot """
        self.current_hits.append(self.fired_shots[-1])  # adding last shot to current hits
        self.sink_ship_padded_coords()
        self.current_hits = []  # resetting current hits
        return self.make_random_shot()

    def if_hit(self):
        self.current_hits.append(self.fired_shots[-1])  # adding last shot to current hits
        if len(self.current_hits) > 1:  # if enemy ship got hit more than once
            self.current_hits.sort()
            self.establish_direction()
            # create possible next shots for established direction and check if they are on board
            self.current_possible_coords = {x for x in [
                (self.current_hits[0][0] - self.current_ship_direction[0],
                 self.current_hits[0][1] - self.current_ship_direction[1]),
                (self.current_hits[-1][0] + self.current_ship_direction[0],
                 self.current_hits[-1][1] + self.current_ship_direction[1])
            ] if val.is_coord_on_board(self.board, x) and x not in self.fired_shots}
        else:  # if first hit on new enemy ship
            self.set_next_possible_coords()
        self.fired_shots.append(self.current_possible_coords.pop())
        return self.fired_shots[-1]

    def if_miss(self):
        if len(self.current_possible_coords) == 0:
            return self.make_random_shot()
        self.fired_shots.append(self.current_possible_coords.pop())
        return self.fired_shots[-1]

    def make_random_shot(self):
        """ Get random shot and check against self.fired_shots """
        r_s = gm.get_random_str_coord(self.board)
        while val.str_coord_to_index_coord(self.board, r_s) in self.fired_shots:
            r_s = gm.get_random_str_coord(self.board)
        r_s_idx = val.str_coord_to_index_coord(self.board, r_s)
        self.fired_shots.append(r_s_idx)
        return self.fired_shots[-1]

    def set_next_possible_coords(self):
        """ Add next possible ship shots based on last hit from self.current_hits"""
        next_coords = [(self.current_hits[-1][0] - 1, self.current_hits[-1][1]),
                       (self.current_hits[-1][0] + 1, self.current_hits[-1][1]),
                       (self.current_hits[-1][0], self.current_hits[-1][1] - 1),
                       (self.current_hits[-1][0], self.current_hits[-1][1] + 1)]
        self.current_possible_coords.update([x for x in next_coords if
                                             val.is_coord_on_board(self.board, x) and x not in self.fired_shots])

    def establish_direction(self):
        """ Establish enemy ship direction base on difference in x or y of hits"""
        if self.current_hits[-1][0] - self.current_hits[-2][0] in [1, -1]:
            self.current_ship_direction = (1, 0)
        else:
            self.current_ship_direction = (0, 1)

    def sink_ship_padded_coords(self):
        """ Adds padded coords of sank ship to fired shots """
        c_h_padded_nest = [val.get_padded_coords_for_coord(x) for x in self.current_hits]  # getting nested pad coords
        c_h_padded = {x for y in c_h_padded_nest for x in y if
                      val.is_coord_on_board(self.board, x) and x not in self.fired_shots}
        self.fired_shots.extend(list(c_h_padded))  # adding unavailable coords
