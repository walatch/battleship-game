import time
import connection
import game_board
import game_mechanics as gm
import ai
import validators as val


class Game:

    def __init__(self):
        self.own_board = game_board.Board()
        self.enemy_board = game_board.Board()

        self.ships_objects = gm.random_fleet_builder(self.own_board)
        self.own_ships_coords = gm.extract_flat_ship_coords(self.ships_objects)
        self.own_board.ships = self.own_ships_coords

        self.is_human = False  # default set to non human
        self.cpu_gunner = ai.AiGunner(self.enemy_board)

        self.shots_fired = []
        self.shots_fired_result = []
        self.shots_recvd = []
        self.shots_recvd_result = []

        self.total_hp = sum(i.health for i in self.ships_objects)
        self.end_game = False
        self.game_result = None
        self.game_rounds = 0

        self.connection_info = None

        self.protocol = {}

    def get_conn_info_from_user(self):
        """ Getting connection information from local user for setting up server connection socket
        Returns :tuple (:str HOST, :int PORT, :int BUFFER_SIZE, :str ENCODING)"""

        host_addr = input("\nProvide server host address (default 127.0.0.1 for no input): \n")
        host_addr = "127.0.0.1" if host_addr == "" else host_addr
        host_port = input("Provide server port (default 9876 for no input): \n")
        host_port = 9876 if host_port == "" else int(host_port)
        host_buff = input("Provide packets buffer size (default 512 for no input): \n")
        host_buff = 512 if host_buff == "" else int(host_buff)
        host_encoding = input("Provide encoding style for packets (default utf-8 for no input): \n")
        host_encoding = "utf-8" if host_encoding == "" else host_encoding
        self.connection_info = (host_addr, host_port, host_buff, host_encoding)

    def start_game(self):
        """ Game starter method for choosing app mode: human / ai_server / ai_client """
        modes = {"HUMAN": "CLIENT",
                 "AI_CLIENT": "CLIENT",
                 "AI_SERVER": "SERVER"}
        selected_mode = val.strip_and_caps_str(input("Modes: HUMAN / AI_SERVER / AI_CLIENT : \n"))
        while selected_mode not in ["HUMAN", "AI_SERVER", "AI_CLIENT"]:
            selected_mode = val.strip_and_caps_str(input("Modes: HUMAN / AI_SERVER / AI_CLIENT : \n"))

        if selected_mode == "HUMAN":
            self.is_human = True
        self.game(modes[selected_mode])

    def game(self, mode):
        """ Starts main game of a given mode """
        self.get_conn_info_from_user()
        print("\n. . . Waiting for connection . . . \n")
        with connection.Connection(*self.connection_info, mode) as conn:
            self.print_e_egg(conn)
            if mode == "CLIENT":
                recvd_data = conn.recv_data()
                self.make_first_response(recvd_data)
            else:
                self.make_first_shot()
            conn.send_json(self.protocol)

            # main game loop after first shot
            while not self.end_game:
                self.game_rounds += 1
                print(f"\n{self.game_rounds=} - {self.total_hp=}")
                recvd_data = conn.recv_data()
                self.app_round(recvd_data)
                conn.send_json(self.build_response())
                if not recvd_data.get("end_game", False) and self.end_game:
                    recvd_data = conn.recv_data()
                    self.enemy_board.ships = recvd_data["own_ships"]
                print("")
            self.print_result()
            if not self.is_human:
                self.replay()

    def add_enemy_shot(self, coord):
        for a in [self.own_board.shots, self.shots_recvd]:
            a.append(coord)

    def add_own_shot(self, coord):
        for a in [self.enemy_board.shots, self.shots_fired]:
            a.append(coord)

    def draw_board(self, board):
        board.mark_ships()
        board.mark_shots()
        board.draw_board()

    def human_draw_boards(self):
        print("Your board:")
        self.draw_board(self.own_board)
        print("Enemy board:")
        self.draw_board(self.enemy_board)

    def make_first_response(self, recvd_data):
        # dealing with cli shot
        self.add_enemy_shot(recvd_data["shot"])
        # dealing with cli shot result
        cli_shot_result = self.check_hit_result()
        self.shots_recvd_result.append(cli_shot_result)
        self.protocol["shot_result"] = cli_shot_result
        # making first shot
        if self.is_human:
            self.human_draw_boards()
            cli_shot = val.strip_and_caps_str(input("Enter shot: "))
            self.add_own_shot(cli_shot)
        else:
            ai_shot = self.cpu_gunner.next_shot()
            self.add_own_shot(ai_shot)
        self.protocol["shot"] = self.shots_fired[-1]

    def make_first_shot(self):
        first_shot = val.strip_and_caps_str(input("Enter shot: ")) if self.is_human else self.cpu_gunner.next_shot()
        self.add_own_shot(first_shot)
        self.protocol["shot"] = first_shot

    def app_round(self, recvd_data):
        self.check_if_rival_lost(recvd_data)
        self.deal_with_response(recvd_data)
        self.make_own_move()
        self.check_if_app_lost()

    def make_own_move(self):
        if not self.end_game:
            if self.is_human:
                next_shot = val.strip_and_caps_str(input("Enter shot: "))
            else:
                self.cpu_gunner.shot_result = self.shots_fired_result[-1]
                next_shot = self.cpu_gunner.next_shot()
            self.add_own_shot(next_shot)

    def deal_with_response(self, recvd_data):
        if self.end_game:
            self.shots_fired_result.append("SINK")
            self.enemy_board.ships.append(self.shots_fired[-1])
        else:
            self.add_enemy_shot(recvd_data.get("shot"))
            self.shots_fired_result.append(recvd_data.get("shot_result"))
            if recvd_data.get("shot_result") in ["HIT", "SINK"]:
                self.enemy_board.ships.append(self.shots_fired[-1])
            cli_shot_result = self.check_hit_result()
            self.shots_recvd_result.append(cli_shot_result)
        if self.is_human:
            self.human_draw_boards()
        print(f"Last shot at {self.shots_fired[-1]} and the result was a {self.shots_fired_result[-1]}")
        if not self.end_game:
            print(f"Rival shot at {self.shots_recvd[-1]} and the result was a {self.shots_recvd_result[-1]}")

    def check_if_rival_lost(self, recvd_data):
        if recvd_data.get("end_game") and not self.game_result:
            self.game_result = "VICTORY IS OURS!"
        self.end_game = recvd_data.get("end_game", False)

    def check_if_app_lost(self):
        if self.total_hp == 0:
            self.end_game = True
        if self.end_game and not self.game_result:
            self.game_result = "ENEMY WON!"

    def check_hit_result(self):
        if self.shots_recvd[-1] not in self.shots_recvd[:-1] and self.shots_recvd[-1] in self.own_board.ships:
            for ship in self.ships_objects:
                if self.shots_recvd[-1] in ship.coords:
                    index = ship.coords.index(self.shots_recvd[-1])
                    ship.coords.pop(index)
                    self.total_hp -= 1
                    if len(ship.coords) == 0:
                        return "SINK"
                    else:
                        return "HIT"
        return "MISS"

    def build_response(self):
        """ Building JSON response to self.protocol attribute"""
        self.protocol = {}
        if self.end_game:
            self.protocol["end_game"] = True
            self.protocol["own_ships"] = self.own_board.ships
        else:
            self.protocol["shot"] = self.shots_fired[-1]
            self.protocol["shot_result"] = self.shots_recvd_result[-1]
        return self.protocol

    def print_e_egg(self, conn):
        if conn.connection_type == "server":
            print(f"\nConnected with client from: {conn.address}")
        else:
            print(f"\nConnected with: {self.connection_info[1]}")
        print("...")
        print("Initializing SKYNET battleship program...")
        print("Ready for puny opponent")
        print("")

    def print_result(self):
        print("")
        print(10 * "-")
        print(self.game_result)
        print(10 * "-")
        print("")

    def replay(self):
        interval = input("Provide round replay delay in seconds: ")
        interval = float(interval) if interval.isnumeric() else 0
        print("...")
        rival_ships = self.enemy_board.ships
        self.own_board.shots = []
        self.enemy_board.shots = []

        for replay_round in range(len(self.shots_recvd)):
            print("")
            print(f"Round: {replay_round + 1}")
            print(f"Own board - client shots & hits")
            self.own_board.reset_board_marks()
            self.own_board.shots.append(self.shots_recvd[replay_round - 1])
            self.own_board.ships = self.own_ships_coords
            self.draw_board(self.own_board)
            print(f"Own shots & hits")
            self.enemy_board.reset_board_marks()
            self.enemy_board.shots.append(self.shots_fired[replay_round - 1])
            self.enemy_board.ships = [x for x in rival_ships if x in self.enemy_board.shots]
            self.draw_board(self.enemy_board)
            print(f"Enemy ships - Own shots & hits")
            self.enemy_board.reset_board_marks()
            self.enemy_board.shots.append(self.shots_fired[replay_round - 1])
            self.enemy_board.ships = rival_ships
            self.draw_board(self.enemy_board)
            print(f"Enemy shots & hits")
            self.own_board.reset_board_marks()
            self.own_board.shots.append(self.shots_recvd[replay_round - 1])
            self.own_board.ships = [x for x in self.own_ships_coords if x in self.own_board.shots]
            self.draw_board(self.own_board)
            time.sleep(interval)

        print("")
        print(self.game_result)
