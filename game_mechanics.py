import time
import random
import ship_generator as sg
import validators as valid


random.seed(time.time())
ship_rules = {"4": 1, "3": 2, "2": 3, "1": 4}
directions = ["N", "S", "W", "E"]


def get_random_str_coord(board):
    """ Returns random string coordinates :tuple based on game board size :tuple"""
    rand_x, rand_y = random.randint(0, board.board_size_index[0]), random.randint(0, board.board_size_index[1])
    return valid.index_coord_to_str_coord(board, (rand_x, rand_y))


def random_fleet_builder(board, num_of_ship=ship_rules):
    """
    Generates random ships based on game rules :dict {"ship_length": num_of_ships}
    and returns :list of Ship :objects
    """
    ai_fleet_ships = []

    for i in num_of_ship.keys():
        y = num_of_ship[i]
        while y > 0:
            random_point = get_random_str_coord(board)
            random_direction = random.choice(directions)

            ship = sg.Ship.ship_builder(board, random_point, random_direction, int(i), ai_fleet_ships)
            if not ship:
                continue
            ai_fleet_ships.append(ship)
            y -= 1

    return ai_fleet_ships


def extract_flat_ship_coords(ai_fleet):
    return sum((x.coords for x in ai_fleet), [])

